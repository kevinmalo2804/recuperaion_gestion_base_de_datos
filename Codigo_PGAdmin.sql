/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     28/1/2022 8:26:02                            */
/*==============================================================*/


drop index RELATIONSHIP_2_FK;

drop index RELATIONSHIP_1_FK;

drop index ALQUILER_PK;

drop table ALQUILER;

drop index CLIENTE_PK;

drop table CLIENTE;

drop index PELICULA_PK;

drop table PELICULA;

/*==============================================================*/
/* Table: ALQUILER                                              */
/*==============================================================*/
create table ALQUILER (
   ID_ANLQUILER         INT4                 not null,
   ID_CLIENTE           INT4                 null,
   ID_PELICULA          INT4                 null,
   FECHA_PRESTAMO       DATE                 not null,
   FECHA_ENTREGA        DATE                 not null,
   VALOR_ALQUILER       NUMERIC(8,2)         not null,
   constraint PK_ALQUILER primary key (ID_ANLQUILER)
);

/*==============================================================*/
/* Index: ALQUILER_PK                                           */
/*==============================================================*/
create unique index ALQUILER_PK on ALQUILER (
ID_ANLQUILER
);

/*==============================================================*/
/* Index: RELATIONSHIP_1_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_1_FK on ALQUILER (
ID_CLIENTE
);

/*==============================================================*/
/* Index: RELATIONSHIP_2_FK                                     */
/*==============================================================*/
create  index RELATIONSHIP_2_FK on ALQUILER (
ID_PELICULA
);

/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE (
   ID_CLIENTE           INT4                 not null,
   CEDULA_CLIENTE       VARCHAR(10)          not null,
   NOMBRE_CLIENTE       VARCHAR(20)          not null,
   APELLIDO_CLIENTE     VARCHAR(20)          not null,
   FECHA_REGISTRO_CLIENTE DATE                 not null,
   constraint PK_CLIENTE primary key (ID_CLIENTE)
);

/*==============================================================*/
/* Index: CLIENTE_PK                                            */
/*==============================================================*/
create unique index CLIENTE_PK on CLIENTE (
ID_CLIENTE
);

/*==============================================================*/
/* Table: PELICULA                                              */
/*==============================================================*/
create table PELICULA (
   ID_PELICULA          INT4                 not null,
   NOMBRE_PELICULA      VARCHAR(30)          not null,
   ANIO_ESTRENO         INT4                 not null,
   constraint PK_PELICULA primary key (ID_PELICULA)
);

/*==============================================================*/
/* Index: PELICULA_PK                                           */
/*==============================================================*/
create unique index PELICULA_PK on PELICULA (
ID_PELICULA
);

alter table ALQUILER
   add constraint FK_ALQUILER_RELATIONS_CLIENTE foreign key (ID_CLIENTE)
      references CLIENTE (ID_CLIENTE)
      on delete restrict on update restrict;

alter table ALQUILER
   add constraint FK_ALQUILER_RELATIONS_PELICULA foreign key (ID_PELICULA)
      references PELICULA (ID_PELICULA)
      on delete restrict on update restrict;
